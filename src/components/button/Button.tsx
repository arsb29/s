import styles from './Button.module.css'


type Props = {
  children: string | number,
  onClick: () => void
}

export function Button(props: Props) {
  return (
    <button onClick={props.onClick} className={styles.button}>{props.children}</button>
  )
}