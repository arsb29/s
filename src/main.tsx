import React from 'react';
import ReactDOM from 'react-dom/client';
import {createBrowserRouter, RouterProvider} from 'react-router-dom';
import {Start} from "./routes/start";
import {Play} from "./routes/play";
import {Win} from "./routes/win";
import './styles.css';


const router = createBrowserRouter([
  {
    path: "/",
    element: <Start/>,
  },
  {
    path: "/play",
    element: <Play/>,
  },
  {
    path: "/win",
    element: <Win/>,
  },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>,
)
