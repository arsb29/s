import Fireworks from '@fireworks-js/react';
import React, {useState} from 'react';
import Typed from 'typed.js';
import {Button} from "../../components/button/Button.tsx";
import styles from './Win.module.css';


const TEXT = '<span>Супеееер, это правильный ответ! Соня, ты выиграла, ииии если я сейчас завлек твое внимание, то не могу упустить такой шанс <br /><br />' +
  'Пусть даже мы и виделись пока один раз, но за это время ты обрела особую значимость для меня. И каждый раз вспоминая тебя, у меня возникают невероятно теплые чувства. Мне хочется дарить эти чувства. Поэтому, я бы хотел пригласить тебя провести вечер 14 февраля</span>'
  + '<br /><br/>ps Можем конечно и книжками обменяться))</span>'
;

export function Win() {
  // Create reference to store the DOM element containing the animation
  const el = React.useRef(null);
  const [showButton, setShowButton] = useState(false);

  React.useEffect(() => {
    const typed = new Typed(el.current, {
      strings: [TEXT],
      typeSpeed: 60
    });
    setTimeout(() => {
      setShowButton(true)
    }, 25000);

    return () => {
      typed.destroy();
    };
  }, []);
  const handleClick = () => window.open('https://t.me/arsb29');

  return (
    <div>
      <div className={styles.fire}><Fireworks options={{intensity: 12}}/></div>
      <div className={styles.text}>
        <div><span ref={el}/></div>
        {showButton && <div><Button onClick={handleClick}>Ответить</Button></div>}
      </div>
    </div>
  );
}