import React from 'react';
import Typed from 'typed.js';
import {useNavigate} from "react-router-dom";
import {Button} from "../../components/button/Button.tsx";
import styles from './Start.module.css'


const TEXT = '<span>Сколько котиков 🐱 сможешь найти?</span>';

export function Start() {
  // Create reference to store the DOM element containing the animation
  const el = React.useRef(null);
  const navigate = useNavigate();

  React.useEffect(() => {
    const typed = new Typed(el.current, {
      strings: [TEXT],
      typeSpeed: 60
    });

    return () => {
      typed.destroy();
    };
  }, [])

  const handleClick = () => navigate('/play');

  return (
    <div className={styles.root}>
      <div>
        <span ref={el}/>

      </div>
      <div>
        <Button onClick={handleClick}>Начать</Button>
      </div>
    </div>
  );
}