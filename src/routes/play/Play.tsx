import styles from './Play.module.css';
import Countdown from "react-countdown";
import {useCallback, useState} from "react";
import {Button} from "../../components/button/Button.tsx";
import {useNavigate} from "react-router-dom";

function randomInteger(min: number, max: number) {
  // случайное число от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

const SECONDS = 10;
const TIME = SECONDS * 1000;

const MIN = randomInteger(50, 35);
const MAX = randomInteger(70, 55);
const COUNT = randomInteger(MIN, MAX);
const CATS = Array.from({length: COUNT}).map((_element) => ({
  left: randomInteger(0, 100),
  top: randomInteger(0, 100)
}));

export function Play() {
  const [timeFinished, setTimeFinished] = useState(false);
  const [date] = useState(Date.now() + TIME);
  const navigate = useNavigate();
  const renderer = useCallback(({seconds, completed}: any) => {
    if (completed) {
      setTimeFinished(true);
    } else {
      return <span>Оставшееся время: {seconds}</span>;
    }
  }, []);

  const handleClick = () => {
    navigate('/win');
  }

  if (timeFinished) {
    return (
      <div className={styles.finished}>
        <div>
          Сколько котиков 🐱 было на экране?
        </div>
        <div className={styles.buttons}>
          <div className={styles.button}><Button onClick={handleClick}>{COUNT - 1}</Button></div>
          <div className={styles.button}><Button onClick={handleClick}>{COUNT}</Button></div>
          <div className={styles.button}><Button onClick={handleClick}>{COUNT + 1}</Button></div>
        </div>
      </div>
    )
  }
  return (
    <div className={styles.area}>
      {CATS.map((position, index) => (
        <div key={index} style={{left: `${position.left}%`, top: `${position.top}%`, position: "absolute"}}>🐱</div>
      ))}
      <div className={styles.countDown}>
        <Countdown date={date} renderer={renderer}/>
      </div>
    </div>
  );
}